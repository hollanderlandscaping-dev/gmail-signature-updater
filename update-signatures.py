
from apiclient import discovery
from bs4 import BeautifulSoup
from httplib2 import Http
from lxml import html
from oauth2client import file, client, tools
from oauth2client.service_account import ServiceAccountCredentials

import json
import copy
import os

# Define scopes used
SCOPES = """https://www.googleapis.com/auth/gmail.settings.basic
     https://www.googleapis.com/auth/admin.directory.user.readonly"""

# Open signature template and create soup object
sig_html = open('signature-template.html', 'r')
TEMPLATE = BeautifulSoup(sig_html.read(), 'html.parser')
sig_html.close()

# Get credentials from storage
store = file.Storage('storage.json')
creds = store.get()

# If no credentials are found, create them from service_account.json
if not creds or creds.invalid:
    creds = ServiceAccountCredentials.from_json_keyfile_name(
        'service_account.json', scopes=SCOPES)
    # Store the credentials
    store.put(creds)

# Create the admin service with admin's credentials
admin_creds = creds.create_delegated("admin@hollanderlandscaping.com")
ADMIN = discovery.build('admin', 'directory_v1', credentials=admin_creds)

# Grab the user list and put them into an array
user_json = ADMIN.users().list(domain='hollanderlandscaping.com').execute()
USERS = user_json['users']

for user in USERS:
    # Initialize variables
    name = user['name']['fullName']
    email = user['primaryEmail']
    title = ''

    # Create delegated credentials for the user
    del_creds = creds.create_delegated(email)

    # Create Gmail service using admin credentials
    GMAIL = discovery.build('gmail', 'v1', credentials=del_creds)

    # This try catches the error if the user does not have a title
    try:
        for organization in user['organizations']:
            if organization['primary']:
                title = organization['title']
    except KeyError:
        title = None

    if title is None:
        # Create data with a blank signature
        data = {'signature': ""}

        # Write the blank signature
        rsp = GMAIL.users().settings().sendAs().patch(
            userId='me', sendAsEmail=email, body=data).execute()

        # Delete signature HTML (if it exists)
        try:
            os.remove("signatures\\" + name + ".html")
            print(name + " does not have a job title anymore. "
                "Their signature is now blank.")
        except FileNotFoundError:
            print(
                name + " does not have a job title. "
                "Their signature is now blank.")

    else:

        # Create a copy of the HTML template to edit
        new_sig = copy.deepcopy(TEMPLATE)

        # Change the HTML to include the user's data
        new_sig.find(id="name").string = name
        email_tag = new_sig.new_tag(
            "a",
            href="mailto:" + email,
            style="""color: #1da1db; text-decoration: none; font-weight: normal;
                font-size: 14px;"""
        )
        email_tag.string = email
        new_sig.find(id="email").string = ""
        new_sig.find(id="email").append(email_tag)
        new_sig.find(id="title").string = title

        # Create a 'pretty' string of the signature
        new_sig_html = new_sig.prettify()

        # Write the user's signature to the 'signatures' subdirectory
        sig_file = open("signatures\\" + name + ".html", 'w')
        sig_file.write(new_sig_html)
        sig_file.close()

        # Create the signature data
        data = {'signature': new_sig_html}

        # Write the new signature and print a confirmation
        rsp = GMAIL.users().settings().sendAs().patch(
            userId='me', sendAsEmail=email, body=data).execute()
        print(name + "\'s signature has been changed successfully.")
