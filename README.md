# Hollander Landscaping Gmail Signature Updater


## About
This script iterates through each user registered to Hollander Landscaping
and updates their Gmail signatures. The user list is grabbed using the
Google Admin SDK. If the user has a job title, their information is put
into the template HTML and their Gmail signature will be updated to
include their new signature. If the user does not have a job title,
their signature will be blank.


## Prerequisites

Make sure git, python3, and pip are installed.

Install the required modules.
* pip
  ```sh
  pip install --upgrade oauth2client lxml google-api-python-client bs4
  ```

## Installation

Clone the repo.
   ```sh
   git clone https://hollanderlandscaping-dev@bitbucket.org/hollanderlandscaping-dev/gmail-signature-updater.git
   ``` 

## Usage

Run the updater script with Python.
   ```sh
   python3 update-signatures.py
   ```

## Author

Ethan Vander Kooi